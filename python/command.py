# -*- coding: utf-8 -*-
#!/usr/bin/env python


################################################################################
# Command line calls abstraction library
################################################################################


import getpass
import subprocess
from myprint import Print
from os import getcwd
import time
import threading
import Queue

#
# Why two run* commands ?
# Because I didn't find a clean way to do all that at the same time :
#  - send data through stdin pipe
#  - run the command with sudo if needed
#  - write all lines (stderr and strout) in real time in console and/or in a file
#  - get the output in a python variable
#
# All together can lead to at least one of these problems :
#  - deadlock in subprocesses pipes
#  - (code, out, err) are all empty
#
# Also, when you run a shell command or a program you usualy need either :
#  - to get the result of that command as a string or a list or something similar (runget)
#  - to simply run the command (runlog)
# Those two simple cases are managed, there is no need to manage other cases right now.
#
# If you know a better way to do that, please let me know (remi.savoye@parrot.com).
#


class Command():
	def __init__(self, path=getcwd(), ask_sudo_first=False):
		self.pw = None
		self.cwd = [getcwd()]
		if ask_sudo_first:
			self.askpass()

	def pushd(self, path):
		"""Change the working path, similar to pushd bash builtin"""
		Print.debug("Changing directory to %s" % path)
		self.cwd.append(path)

	def popd(self):
		"""Pop the last directory from working path list, similar to popd bash builtin"""
		res = self.cwd.pop()
		if len(self.cwd) == 0:
			self.cwd = [getcwd()]
			Print.debug('No more directory in path list, added %s' % self.cwd[-1])
		return res

	def askpass(self):
		"""Ask the user his sudo password for later use"""
		ok = False
		self.pw = None
		while not ok:
			try:
				self.pw = getpass.getpass("sudo password = ")
			except KeyboardInterrupt:
				Print.error_and_exit("This script needs a root access")
			proc=subprocess.Popen('sudo -S whoami', shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
			outline, errline = proc.communicate(self.pw + '\n')
			ok = proc.wait() == 0
			if not ok:
				Print.warning("wrong password")
		Print.info("Thanks, I have %s access now !" % outline[:-1])
		return True

	def runlog(self, command, sudo=False):
		"""Run a command [with stdin entry], logged into logsystem.
		Returns the return code of the shell command."""
		# This function is able to run any command with or without sudo, and log it
		# some code from http://stefaanlippens.net/python-asynchronous-subprocess-pipe-reading
		Print.debug('Running "' + command + '" with logs from ' + self.cwd[-1])
		if sudo:
			if self.pw == None:
				self.askpass()
			command = 'sudo -S ' + command
			sudopass = self.pw + '\n'

		proc=subprocess.Popen(command, shell=True, stdin={True:subprocess.PIPE,False:None}[sudo], stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=self.cwd[-1])

		if sudo:
			proc.stdin.write(sudopass)

		# Launch the asynchronous readers of the process' stdout and stderr.
		stdout_queue = Queue.Queue()
		stdout_reader = AsynchronousFileReader(proc.stdout, stdout_queue)
		stdout_reader.start()
		stderr_queue = Queue.Queue()
		stderr_reader = AsynchronousFileReader(proc.stderr, stderr_queue)
		stderr_reader.start()

		# Check the queues if we received some output (until there is nothing more to get).
		while not stdout_reader.eof() or not stderr_reader.eof():
			# Show what we received from standard error.
			while not stderr_queue.empty():
				line = stderr_queue.get()
				Print.stderr(line)
			# Show what we received from standard output.
			while not stdout_queue.empty():
				line = stdout_queue.get()
				Print.stdout(line)
			# Sleep a bit before asking the readers again.
			time.sleep(.1)
		# Let's be tidy and join the threads we've started.
		stdout_reader.join()
		stderr_reader.join()
		if sudo:
			proc.stdin.close()

		# Close subprocess' file descriptors.
		proc.stdout.close()
		proc.stderr.close()
		proc.wait()
		if proc.returncode != 0:
			Print.debug('The command "' + command + '" failed with returncode ' + str(proc.returncode))
		return proc.returncode

	def runget(self, command, stdin='', sudo=False):
		"""Run a command [with stdin entry], no log, and return its (return code, stdout, stderr)"""
		# This function is able to run any command with any input stream with or without sudo.
		# Nothing is thrown on the console (except vital information, like sudo call)
		Print.debug('Running "' + command + '" to get its result from ' + self.cwd[-1])
		communicate = stdin
		if stdin:
			communicate = stdin + '\n'
		if sudo:
			if self.pw == None:
				self.askpass()
			command = 'sudo -S ' + command
			communicate = self.pw + '\n' + communicate

		proc=subprocess.Popen(command, shell=True, stdin = subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=self.cwd[-1])

		outline, errline = proc.communicate(communicate)
		Print.debug('result is ' + outline)
		if proc.returncode != 0:
			Print.debug('The command "' + command + '" failed with returncode ' + str(proc.returncode))
		return proc.wait(), outline, errline

	def sudoRun(self, command, stdin=''):
		"""Run a command as a sudo user"""
		return self.run(command, stdin, sudo=True)



class AsynchronousFileReader(threading.Thread):
	'''
	Helper class to implement asynchronous reading of a file
	in a separate thread. Pushes read lines on a queue to
	be consumed in another thread.
	'''
	def __init__(self, fd, queue):
		assert isinstance(queue, Queue.Queue)
		assert callable(fd.readline)
		threading.Thread.__init__(self)
		self._fd = fd
		self._queue = queue

	def run(self):
		'''The body of the tread: read lines and put them on the queue.'''
		for line in iter(self._fd.readline, ''):
			self._queue.put(line)

	def eof(self):
		'''Check whether there is no more content to expect.'''
		return not self.is_alive() and self._queue.empty()

