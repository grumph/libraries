import math

class Point:
	def __init__(self, x, y):
		self.x = x
		self.y = y
	def __str__(self):
		return str(self.x) + ' ' + str(self.y)
	def distance(self, p1, p2=None):
		if p2 is None:
			return math.sqrt((p1.y - self.y) * (p1.y - self.y) + (p1.x - self.x) * (p1.x - self.x))
		else:
			t = Point(p2.x-p1.x, p2.y-p1.y)
			dd = math.sqrt(t.x**2+t.y**2)
			t = Point(t.x/dd, t.y/dd)
			n = Point(-t.y, t.x)
			ac = Point(self.x-p1.x, self.y-p1.y)
			return math.fabs(ac.x*n.x+ac.y*n.y)

def barycentre(points):
	return Point(sum([p.x for p in points]) / len(points),
				 sum([p.y for p in points]) / len(points))

class Circle:
	def __init__(self, center, radius):
		self.center = center
		self.radius = radius
	def __str__(self):
		return str(self.center) + ', r=' + str(self.radius)
	def tangeants(point):
		d = self.center.distance(point)
		a = math.asin(self.radius / d)
		b = math.atan2(self.center.y - point.y, self.center.x - point.x)
		res = [Point(self.center.x + self.radius * math.sin(b - a),
					 self.center.y + self.radius * -math.cos(b - a)),
			   Point(self.center.x + self.radius * -math.sin(b + a),
					 self.center.y + self.radius * math.cos(b + a))]
	def intersection(circle):
		d = self.center.distance(circle.center)
		if d == 0:
			return []
		a = (self.radius ** 2 - circle.radius ** 2  + d ** 2) / (2 * d)
		b = self.radius ** 2 - a ** 2
		if b < 0:
			return []
		h = math.sqrt(b)
		x1 = (circle.center.x - self.center.x) * (a / d) + self.center.x
		y1 = (circle.center.y - self.center.y) * (a / d) + self.center.y
		x2 = h * (circle.center.y - self.center.y) / d
		y2 = h * (circle.center.x - self.center.x) / d
		return [Point(x1 + x2, y1 - y2),
				Point(x1 - x2, y1 + y2)]
