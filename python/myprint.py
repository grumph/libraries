# -*- coding: utf-8 -*-
#!/usr/bin/env python


################################################################################
# Library for printing and logging
################################################################################


import sys
import logging
import utils

from config import LOGFILE

LOG_STDOUT = 21 # logger.INFO is 20
LOG_STDERR = 22

class LoggerManager(utils.Singleton):
	"""
	Logger Manager.
	Handles all logging files.
	"""
	def init(self):
		logging.addLevelName(LOG_STDOUT, 'STDOUT')
		logging.addLevelName(LOG_STDERR, 'STDERR')
		self.logger = logging.getLogger()
		handler = None
		try:
			handler = logging.FileHandler(LOGFILE, "a")
		except:
			raise IOError("Couldn't create/open file \"" + \
						  LOGFILE + "\". Check permissions.")
		self.logger.setLevel(logging.DEBUG)
		formatter = logging.Formatter("%(asctime)s [%(levelname)s] %(message)s")
		handler.setFormatter(formatter)
		self.logger.addHandler(handler)

	def debug(self, msg):
		self.logger = logging.getLogger()
		self.logger.debug(msg)

	def critical(self, msg):
		self.logger = logging.getLogger()
		self.logger.critical(msg)

	def error(self, msg):
		self.logger = logging.getLogger()
		self.logger.error(msg)

	def info(self, msg):
		self.logger = logging.getLogger()
		self.logger.info(msg)

	def warning(self, msg):
		self.logger = logging.getLogger()
		self.logger.warning(msg)

	def stdout(self, msg):
		self.logger = logging.getLogger()
		self.logger.log(LOG_STDOUT, msg)

	def stderr(self, msg):
		self.logger = logging.getLogger()
		self.logger.log(LOG_STDERR, msg)



class Print:
	ENDC = '\033[0m'
	BLUE="\033[1;34m" #reminder
	LIGHT_BLUE="\033[0;34m"
	RED="\033[0;31m"
	GREEN="\033[0;32m"
	YELLOW="\033[1;33m"
	LIGHT_RED="\033[1;31m"
	WHITE="\033[1;37m"
	@classmethod
	def header(self, text):
		lm = LoggerManager()
		lm.info('*'*80)
		lm.info('* ' + text)
		lm.info('*'*80)
		print "\033[1;34m" + text + self.ENDC
	@classmethod
	def info(self, text, log=True):
		LoggerManager().info(text)
		print " [\033[1;33mINFO" + self.ENDC + '] ' + text
	@classmethod
	def warning(self, text, log=True):
		LoggerManager().warning(text)
		print "\033[31mWarning : " + text + self.ENDC
	@classmethod
	def normal(self, text):
		LoggerManager().stdout(text)
		print text
	@classmethod
	def debug(self, text):
		LoggerManager().debug(text)
#		print " [\033[0;34mDEBUG" + self.ENDC + "] " + text
	@classmethod
	def error(self, text):
		LoggerManager().error(text)
		print "\033[0;31mERROR : " + text + self.ENDC
	@classmethod
	def error_and_exit(self, text):
		LoggerManager().critical(text)
		print "\033[0;31mERROR : " + text + self.ENDC
		exit(1)
	@classmethod
	def stderr(self, text):
		if text[-1] == '\n':
			LoggerManager().stderr(text[:-1])
		else:
			LoggerManager().stderr(text)
		sys.stderr.write(text)
	@classmethod
	def stdout(self, text):
		if text[-1] == '\n':
			LoggerManager().stdout(text[:-1])
		else:
			LoggerManager().stdout(text)
		sys.stdout.write(text)


if __name__ == "__main__" :
	Print.header("Starting the printing tests")
	Print.info("for your information, it's working")
	Print.normal("I can handle normal prints")
	Print.debug("I also handle debugs")
	Print.warning("there is no problem here")
	Print.stderr("this may be an error\n")
	Print.stdout("this is not an error\n")
	Print.error("let's try an error")
	Print.error_and_exit("And now we exit not properly")
