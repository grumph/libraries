from __future__ import print_function
import map_square_data
import astar
import sys
import time

DEBUG=True

def debug(message, *args):
    if DEBUG:
        print(message % args, file=sys.stderr)

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y
    def __str__(self):
        return '(' + str(self.x) + ',' + str(self.y) + ')'
    def __eq__(self, point):
        return isinstance(point, Point) and self.x == point.x and self.y == point.y
    def distance(self, point):
        return abs(point.x - self.x) + abs(point.y - self.y)

class Tile(Point):
    def __init__(self, x, y, c):
        Point.__init__(self, x, y)
        self.c = c
    def __str__(self):
        return self.c
    def is_obstacle(self):
        return self.c == '#'
    def difficulty(self):
        return -1 if self.c == '#' else 0
    def is_start(self):
        return self.c == 'O'
    def is_dest(self):
        return self.c == 'D'

class Labyrinth:
    def __init__(self, tab, diagonals=False):
        self.y = len(tab)
        self.x = len(tab[0])
        self.diagonals = diagonals
        self.carte = []
        for y, row in enumerate(tab):
            self.carte.append([])
            for x, c in enumerate(row):
                self.carte[-1].append(Tile(x, y, c))
                if self.carte[-1][-1].is_start():
                    self.start = self.carte[-1][-1]
                if self.carte[-1][-1].is_dest():
                    self.dest = self.carte[-1][-1]
    def __str__(self):
        res = "Labyrinthe (%i, %i)\n" % (self.x, self.y)
        res += '#' * (self.x + 2) + '\n'
        for row in self.carte:
            res += '#'
            for tile in row:
                res += str(tile)
            res += '#\n'
        res += '#' * (self.x + 2) + '\n'
        return res
    def neighbours(self, tile):
        res = []
        neighbours = [(0, 1, 10),
                      (0, -1, 10),
                      (1, 0, 10),
                      (-1, 0, 10)]
        if self.diagonals:
            neighbours += [(1, 1, 14),
                           (1, -1, 14),
                           (-1, 1, 14),
                           (-1, -1, 14)]
        for x, y, difficulty in neighbours:
            if (tile.x + x >= 0 and tile.x + x < self.x and
                tile.y + y >= 0 and tile.y + y < self.y):
                res.append((self.carte[tile.y + y][tile.x + x], difficulty))
        return res


labs = []
for x in range(12):
    # without diagonals
    labs.append(Labyrinth(getattr(map_square_data, "lab" + str(x + 1))))
    # with diagonals
    labs.append(Labyrinth(getattr(map_square_data, "lab" + str(x + 1)), True))

for l, lab in enumerate(labs):
    print(l)
    t = time.time()
    pth = astar.path(lab.start, lab.dest, lab)
    if pth:
        for tile in pth:
            if not tile.is_start() and not tile.is_dest():
                tile.c = '.'
    print(time.time() - t)
    print(lab)
