import debug

class ATile:
    def __init__(self, tile):
        self.tile = tile
        self.parent = None
        self.score = 0
        self.difficulty = 0
    def __str__(self):
        return str(self.tile) + ': difficulty=%i, score=%i' % (self.difficulty, self.score)
    def __eq__(self, atile):
        return isinstance(atile, ATile) and self.tile == atile.tile
    def neighbours(self, lab):
        debug.TODO("transform to generator")
        res = []
        for n, d in lab.neighbours(self.tile):
            if not n.is_obstacle():
                res.append(ATile(n))
                res[-1].difficulty = d + n.difficulty()
        return res

def path(origin, destination, labyrinth):
    openlist, closedlist = [ATile(origin)], []
    while ATile(destination) not in closedlist:
        # we manage the most probable path, and check its neighbours
        if not openlist:
            return None
        neighbours = openlist[0].neighbours(labyrinth)
        for n in (m for m in neighbours if m not in closedlist):
            if n not in openlist:
                # new neighbour, we add it
                n.parent = openlist[0]
                n.difficulty += n.parent.difficulty
                openlist.append(n)
                res = openlist[-1]
            else:
                # this tile already exists, so we check if it is faster to get there by this path
                res = openlist[openlist.index(n)]
                if n.difficulty + openlist[0].difficulty < res.difficulty:
                    res.parent = openlist[0]
                    res.difficulty = n.difficulty + openlist[0].difficulty
            res.score = res.difficulty + (res.tile.distance(destination) * 10)
        # tile is managed, delete it
        closedlist.append(openlist[0])
        del(openlist[0])
        # and sort the list to get the next most probable path (lowest score first)
        openlist.sort(key=lambda t : t.score)
    path = [closedlist[closedlist.index(ATile(destination))]]
    while path[0].parent != None:
        path = [path[0].parent] + path
    return [p.tile for p in path]
