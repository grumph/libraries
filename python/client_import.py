# -*- coding: utf-8 -*-
#!/usr/bin/env python

################################################################################
# clients/client_*.py importation library
################################################################################


from myprint import Print
import utils
import os, glob, imp


class ImportedClients(utils.Singleton):
	def init(self):
		"""
		This function imports all client_*.py found in the clients folder.
		That way makes it easier to add clients :
		 - add a python file in the clients dir
		 - now you can add the client's name in the client default list in the config
		code from http://www.daniweb.com/software-development/python/threads/238229/import-all-of-folder
		returns a dict where key is the client name and value is the class object
		"""
		Print.info('Import the client modules')
		realpath = os.path.dirname(os.path.realpath(__file__))
		Print.debug('Checking in ' + realpath)
		modules_imported = {}
		for path in glob.glob(realpath + '/clients/client_*.py'):
			Print.debug('importing ' + path)
			name, ext = os.path.splitext(os.path.basename(path))
			modules_imported[name] = imp.load_source(name, path)
			Print.debug(path + ' imported')
		#Now we get the client name and the associated class
		self.clients = {}
		for key in modules_imported: #from imported modules
			for classname in [x for x in dir(modules_imported[key]) if x[0:6] == 'Client' and x != 'Client']: #we get the classes starting with "Client"
				foundclass = getattr(modules_imported[key], classname) #get the class object
				self.clients[foundclass.get_client_name()] = foundclass
				Print.debug(key + ' : found class ' + classname + ' (for client ' + foundclass.get_client_name() + ')')

	def is_valid_client(self, client):
		for key in self.clients:
			if client == key:
				return True
		return False
