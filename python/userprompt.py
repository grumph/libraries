# -*- coding: utf-8 -*-
#!/usr/bin/env python

################################################################################
# Library for easy command prompt
################################################################################

import getpass
import re
from myprint import Print

def ask_user(question, check_function, default_answer=None, secret=False):
	"""
	this function will show a prompt asking 'question' to the user.
	if the user does not answer, 'default_answer' will be returned
	if the user answers something, the function will call check_function(something), which is a function returning True if the answer is valid and False otherwise.
	While the answer of the user is not valid, the question will be asked again. You might print the reason the answer was rejected in check_function.
	if 'secret' is set, the answer will not be shown when typing it (typically passwords prompts)
	Returns the valid answer.
	example : user_email = ask_user('Write your email', is_email, 'default@default.com')
	"""
	while True:
		try:
			ask = question
			if default_answer:
				ask += ' [' + default_answer + ']'
			if secret:
				answer = getpass.getpass('\033[0;32m' + ask + ' :\033[0m ')
			else:
				answer = raw_input('\033[0;32m' + ask + ' :\033[0m ')
			if not answer:
				answer = default_answer
			if not answer: # no user answer and no default_answer
				Print.warning('You must answer something, there is no default')
			elif check_function(answer):
				break
		except (KeyboardInterrupt, EOFError):
			Print.error_and_exit('Abording')
	return answer

def ask_user_yes_no(question, default_yes=True):
	"""
	This function is an example of ask_user usage. It's also a usual case.
	example : is_user_happy = ask_user_yes_no('Are you happy', True)
	"""
	res = ask_user(question, check_yes_no, {True:'yes', False:'no'}[default_yes])
	return res in ['yes', 'y']

def check_yes_no(yes_no):
	return yes_no and yes_no.lower() in ['yes', 'no', 'y', 'n']

def is_email(email):
	if not email or not re.compile('^[^@]+@[^@]+\.[^@]+$').match(email):
		return False
	return True



class CommandPrompt:
	def __init__(self, commandlist, prompt='What to do ?', help_message=''):
		self.commands = {}
		for (command, function, message) in commandlist:
			self.commands[command]={'cmd':function, 'msg':message}
		self.commands['?']={'cmd':self.printCommands, 'msg':'List all commands'}
		this.help_message = help_message
	def printCommands(self, params):
		Print.normal(this.help_message)
		size = len(max(self.console.keys(), key=len))
		for key in sorted(self.console.iterkeys()):
			Print.normal(key +  ":" +  " "*(size - len(key)) + self.console[key]['msg'])
	def console(self):
		while True:
			try:
				print
				ligne = raw_input('\033[0;32' + self.prompt + ' (try "?" for help):\033[0m ')
				try:
					command, params = ligne.split(None,1)
				except ValueError:
					command = ligne
					params = None
				self.console[command.lower()]['cmd'](params)
			except KeyError:
				Print.warning('%s : unknown command. Try "?" for a list of commands'%command)
			except KeyboardInterrupt:
				Print.warning('Command cancelled')
			except EOFError:
				Print.error_and_exit('Aborted')
